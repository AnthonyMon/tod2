package binome;

public class Fraction {
	
	private double numerateur;
	private double denominateur;
	
	public static final double UN = 1;
	public static final double ZERO = 0;
	
	public Fraction (double inum, double idenom) {
		this.numerateur = inum;
		this.denominateur = idenom;
	}
	
	public Fraction (double inum) {
		this.numerateur = inum;
		this.denominateur = 1;
	}
	
	public Fraction() {
		this.numerateur = 0;
		this.denominateur = 1;
	}
	
	public double consulter_fraction() {
		return (this.numerateur/this.denominateur);
	}
	
	public String recuperer_fraction() {
		String str = String.valueOf(this.consulter_fraction());
		return str;
	}
			
	public double get_numerateur()
			  {
				  return this.numerateur;				  
			  }
	
	public double get_denominateur()
			  {
				  return this.denominateur;				  
			  }
	
	public void fraction_au_carre()
	  {
		//fonction bonus
		this.numerateur *= this.numerateur;
		this.denominateur *= this.denominateur;
	  }
	
	public boolean denominateur_correct()
	  {
		//fonction bonus
		if(this.denominateur != 0)
		{return true;}
		return false;
	  }
	
	
	
	public boolean test_egalite(Fraction F)
	{
		if(this.denominateur<F.denominateur)
		{
			if(F.numerateur%this.denominateur == this.numerateur &&
					F.denominateur%this.denominateur == this.denominateur)
				return true;
			
		}
		else {
			if(this.numerateur%F.denominateur == F.numerateur &&
					this.denominateur%F.denominateur == F.denominateur)
				return true;
		
		}
		return false;
	}
	
}
