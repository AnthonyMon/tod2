/*package all;*/

public class Compte 
{
	private int solde;
	private int credit;
	
	//constructeur
	public Compte(int solde) 
	  {
		   this.solde = solde;
	  }
	
	public Compte() 
	  {
		   this.solde = 0;
	  }
	
	//fct
	public int get_solde()
	  {
		  return this.solde;				  
	  }
	
	public void avoir_credit(int credit)
	  {
		  this.credit += credit;		  
	  }
	
	public void avoir_debit(int debit)
	  {
		if(debit > 0)
		  {this.solde += debit;} 
		  else System.out.println("debit negatif erreur");
	  }
	
	public void fournir_debit(int debit)
	  {if(debit > 0)
		  {
			int i = this.solde - debit;
			if(i > 0)
			  {this.solde -= debit;}
			else 
				 System.out.println("debit imposible");
		  }
	  else System.out.println("debit negatif erreur");
	  }
	
	public void transfere(int valeur,Compte receveur)
	  {
		int test = this.get_solde();
		
		this.fournir_debit(valeur);
			if(test == this.get_solde())
			{
		receveur.avoir_debit(valeur);
				System.out.println("transfere terminé");
			}
	  }
}
